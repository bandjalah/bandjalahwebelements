/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.bandjalah.web.elements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Luis Paulo Silva (Bandjalah) <lps.nop@gmail.com
 */
public class ElementAttribute {
    private String attributeName = null;
    private List<String> attributeElements = null;
    
    public ElementAttribute(String attrName){
        this(attrName,(String[])null);
    }
    
    public ElementAttribute(String attrName, String... attrContent){
        setAttributeName(attrName);
        setAttributeElementsList(attrContent == null ? new ArrayList() : Arrays.asList(attrContent));
    }
    
    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public List<String> getAttributeElements() {
        return attributeElements;
    }

    public void setAttributeElementsList(List<String> attributeElements) {
        this.attributeElements = attributeElements;
    }
    
    public void setAttributeElement(String element){
        getAttributeElements().clear();
        getAttributeElements().add(element);
    }
    
    public void addAttributeElement(String element){
        getAttributeElements().add(element);
    }
    
    @Override
    public String toString (){
        String elems = "";
        
        for(String str : getAttributeElements()){
            elems += str + " ";
        }
        
        return String.format("%s = \"%s\"", getAttributeName(), elems);
    }
    
    
}
