/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.bandjalah.web.elements;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Luis Paulo Silva (Bandjalah) <lps.nop@gmail.com>
 */
public interface IElement {
    public void append(IElement element);
    public void appendTo(final IElement element);
    public String getHtml();
    public List<IElement> getChildren();
    public String getContent();
    public void setContent(String content);
    public String getTag();
    public void setTag(String tag);
    public HashMap<String, ElementAttribute> getAttributesMap();
    public String getAttributes();
    public void setAttribute(String attr, String value);
    public void removeAttribute(String attr);
}
