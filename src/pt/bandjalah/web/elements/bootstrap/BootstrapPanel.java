/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.bandjalah.web.elements.bootstrap;

import pt.bandjalah.web.elements.AbstractElement;
import pt.bandjalah.web.elements.Element;
import pt.bandjalah.web.elements.IElement;

/**
 *
 * @author Luis Paulo Silva (Bandjalah) <lps.nop@gmail.com
 */
public class BootstrapPanel extends AbstractElement{
    
    private final String PANEL_HEADER = "panel-heading";
    private final String  PANEL_BODY = "panel-body";
    private final String  PANEL_FOOTER = "panel-footer";
    private final String  PANEL_DEFAULT = "panel panel-default";
    private final String  PANEL_CONFIRM = "panel panel-confirm";
    private final String  PANEL_SUCCESS = "panel panel-success";
    private final String  PANEL_PRIMARY = "panel panel-primary";
    private final String  PANEL_INFO = "panel panel-info";
    private final String  PANEL_WARNING = "panel panel-warning";
    private final String  PANEL_DANGER = "panel panel-danger";
    
    public BootstrapPanel(){
        this(null);
        init(null,null,null);
    }
    
    public BootstrapPanel(String content){
        super("div");
        init(content,null,null);
    }
    
    public BootstrapPanel(String content, String header, String footer){
        super("div");
        init(content, header, footer);
        
    }
    
    private void init(String content, String header, String footer){
        this.setAttribute("class", PANEL_SUCCESS);
        IElement headerElement = new Element("div",header);
        
        headerElement.setAttribute("class", PANEL_HEADER);
        IElement contentElement = new Element("div",content);
        contentElement.setAttribute("class", PANEL_BODY);
        
        IElement footerElement = new Element("div",footer);
        footerElement.setAttribute("class", PANEL_FOOTER);
        
        if(header != null){
            this.append(headerElement);
        }
        if(header != null){
            this.append(contentElement);
        }
        if(header != null){
            this.append(footerElement);
        }
    }
    
    
}
