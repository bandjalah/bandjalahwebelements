/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.bandjalah.web.elements;

/**
 *
 * @author Luis Paulo Silva (Bandjalah) <lps.nop@gmail.com>
 */
public class Element extends AbstractElement{

    public Element(String tag) {
        super(tag);
    }
    
    public Element(String tag, String content){
        super(tag,content);
    }
    
}
