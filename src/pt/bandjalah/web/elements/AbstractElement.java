/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.bandjalah.web.elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Luis Paulo Silva (Bandjalah) <lps.nop@gmail.com
 */
public abstract class AbstractElement implements IElement{
    
    protected List<IElement> children;
    private String tag = null;
    private String content = null;
    private HashMap<String,ElementAttribute> attributes = null;
    
    protected AbstractElement(String tag){
        this(tag,null);
    }
    
    protected AbstractElement(String tag, String content){
        init();
        setTag(tag);
        setContent(content);
    }    

    @Override
    public HashMap<String, ElementAttribute> getAttributesMap() {
        return attributes;
    }

    public void setAttributes(HashMap<String, ElementAttribute> attributes) {
        this.attributes = attributes;
    }
    
    private void init(){
        this.children = new ArrayList<>();
        this.attributes = new HashMap<>();
    }

    @Override
    public void append(IElement element) {
        this.children.add(element);
    }

    @Override
    public void appendTo(final IElement element) {
        element.append(this);
    }

    @Override
    public List<IElement> getChildren() {
        return this.children;
    }

    @Override
    public String getContent() {
        return this.content;
    }
    
    @Override
    public void setContent(String content){
        this.content = content;
    }

    @Override
    public String getTag() {
        return this.tag;
    }
    
    public void setTag(String tag){
        this.tag = tag;
    }

    @Override
    public String getAttributes() {
        String attrs = "";
        
        if(getAttributesMap().size() > 0){
            for(ElementAttribute attr : getAttributesMap().values()){
                attrs += attr.toString();
            }
        }
        
        return attrs;
    }
    
    @Override
    public void setAttribute(String attr, String value){
        
        if(getAttributesMap().containsKey(attr)){
            getAttributesMap().get(attr).setAttributeElement(value);
        }else {
            getAttributesMap().put(attr, new ElementAttribute(attr,value));
        }        
    }
    
    @Override
    public void removeAttribute(String attr){
        if(getAttributesMap().containsKey(attr)){
            getAttributesMap().remove(attr);
        }
    }
    
    @Override
    public String getHtml() {
        return this.toString();
    }
    
    @Override
    public String toString(){
        String childrenHtml = "";
        
        if(getChildren().size() > 0){
            for(IElement element : getChildren()){
                childrenHtml += element.toString();
            }
        }
        
        return String.format("<%s %s>%s%s</%s>", 
                getTag() == null ? "" : getTag(), 
                getAttributes() == null ? "" : getAttributes(), 
                getContent() == null ? "" : getContent(), 
                childrenHtml == null ? "" : childrenHtml, 
                getTag() == null ? "" : getTag());
    }

}
